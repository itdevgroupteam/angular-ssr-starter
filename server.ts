import 'zone.js/dist/zone-node';
import 'reflect-metadata';
import { enableProdMode } from '@angular/core';
import { ngExpressEngine } from '@nguniversal/express-engine';
import { provideModuleMap } from '@nguniversal/module-map-ngfactory-loader';
import { join } from 'path';

import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as cors from 'cors';
import * as compression from 'compression';

const cache = require('memory-cache');
const fs = require('fs');
const files = fs.readdirSync(`${process.cwd()}/dist/server`);
const memCache = new cache.Cache();
const port = process.env.PORT || 8080;

// jquery and DOM-related polyfills
const domino = require('domino');
const template = fs.readFileSync(`${process.cwd()}/dist/browser/index.html`).toString();
const win = domino.createWindow(template);
global['window'] = win;
Object.defineProperty(win.document.body.style, 'transform', {
  value: () => {
    return {
      enumerable: true,
      configurable: true
    };
  },
});
global['document'] = win.document;
global['CSS'] = null;
// global['XMLHttpRequest'] = require('xmlhttprequest').XMLHttpRequest;
global['Prism'] = null;
global['navigator'] = {'userAgent': ''};
const $ = null;
const jQuery = null;

enableProdMode();

export const app = express();

app.use(compression());
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

const cacheMiddleware = (duration) => {
  return (req, res, next) => {
    const key = '__express__' + req.originalUrl || req.url;
    const cacheContent = memCache.get(key);
    if (cacheContent) {
      res.send(cacheContent);
      return;
    } else {
      res.sendResponse = res.send;
      res.send = (body) => {
        memCache.put(key, body, duration * 1000);
        res.sendResponse(body);
      };
      next();
    }
  };
};

// const DIST_FOLDER = join(process.cwd(), 'dist');
const mainFiles = files.filter(file => file.startsWith('main'));
const hash = mainFiles[0].split('.')[1];
const {AppServerModuleNgFactory, LAZY_MODULE_MAP} = require(`./dist/server/main.${hash}`);

app.engine('html', ngExpressEngine({
  bootstrap: AppServerModuleNgFactory,
  providers: [
    provideModuleMap(LAZY_MODULE_MAP)
  ]
}));

app.set('view engine', 'html');
app.set('views', './dist/browser');

app.get('/redirect/**', (req, res) => {
  const location = req.url.substring(10);
  res.redirect(301, location);
});

app.get('*.*', express.static('./dist/browser', {
  maxAge: '1y'
}));

app.get('/robots.txt', (req, res) => {
  const domain = req.headers.host.split('.')[0];
  res.send(`<pre>User-agent: * /n Disallow: *</pre>`);
});

app.get('*', cacheMiddleware(600), (req, res) => {
  const http = req.headers['x-forwarded-proto'] === undefined ? 'http' : req.headers['x-forwarded-proto'];
  renderApp(req, res);
});

function renderApp(req, res) {
  const http = req.headers['x-forwarded-proto'] === undefined ? 'http' : req.headers['x-forwarded-proto'];
  const domain = req.headers.host.split('.')[0];

  return res.render(
    './index',
    <any> {
      req: req,
      res: res,
      providers: [
        {
          provide: 'REQUEST',
          useValue: req
        },
        {
          provide: 'RESPONSE',
          useValue: res
        },
        {
          provide: 'ORIGIN_URL',
          useValue: (`${http}://${req.headers.host}`)
        },
        {
          provide: 'PATH_URL',
          useValue: (req.originalUrl)
        }
      ]
    }
  )/* || errorHandler(null, req, res)*/;
}

app.listen(port, () => {
  console.log('Listening on: http://localhost:' + port);
});

