import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TestPageComponent } from './components/test-page/test-page.component';
import { ExampleDataResolver } from './resolvers/example-data.resolver';

const routes: Routes = [
  {
    path: 'test',
    component: TestPageComponent,
    resolve: {
      data: ExampleDataResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
