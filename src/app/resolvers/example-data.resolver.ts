import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {ApiService} from '../services/api.service';

@Injectable()
export class ExampleDataResolver implements Resolve<any>{
  constructor(private api: ApiService) {}
  resolve(route: ActivatedRouteSnapshot) {
    return this.api.getData();
  }
}
