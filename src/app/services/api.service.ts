import {Injectable} from '@angular/core';
import {TransferHttpService} from '@gorniv/ngx-transfer-http';
import {take} from 'rxjs/operators';

/*
* This is sample http service
* Main difference from non-SSR approach is replacement of HttpClient by TransferHttpService
*
* WARNING: TransferHttpService won't set header data to transfer state
* If you use data coming from headers, consider writing a custom solution
*/

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: TransferHttpService) {
  }

  getData() {
    return this.http.get('https://jsonplaceholder.typicode.com/photos')
      .pipe(take(1));
  }

}
