import { NgtUniversalModule } from '@ng-toolkit/universal';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { TransferHttpModule } from '@gorniv/ngx-transfer-http';
import { ExampleDataResolver } from './resolvers/example-data.resolver';
import { TestPageComponent } from './components/test-page/test-page.component';
import { SharedModule } from './shared/shared.module';
import { environment } from '../environments/environment';
import { ServerMockModule } from './server-mock/server-mock.module';

export function initApp() {
  return () => Promise.resolve();
}

@NgModule({
  declarations: [
    AppComponent,
    TestPageComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'app-root' }),
    CommonModule,
    NgtUniversalModule,
    AppRoutingModule,
    HttpClientModule,
    TransferHttpModule,

    SharedModule,
    environment.isServer ? [
      ServerMockModule
    ] : [
      // dependencies not supported by Universal
    ]
  ],
  providers: [
    ExampleDataResolver,
    {
      provide: APP_INITIALIZER, useFactory: initApp, deps: [], multi: true
    }
  ],
})
export class AppModule {
}
